
import java.util.Set;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná DAINF - Departamento
 * Acadêmico de Informática
 *
 * Template de projeto de programa Java usando Maven.
 *
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica61 {

    public static void main(String[] args) {
        Time time1 = new Time();
        Time time2 = new Time();

        time1.addJogador("Atacante", new Jogador(12, "Pedro"));
        time1.addJogador("Goleiro", new Jogador(13, "Jorge"));
        time1.addJogador("Lateral", new Jogador(14, "Luana"));

        time2.addJogador("Atacante", new Jogador(1, "Amauri"));
        time2.addJogador("Goleiro", new Jogador(2, "Luciana"));
        time2.addJogador("Lateral", new Jogador(3, "Everaldo"));

        time1.compara(time2);
    }
}
